class ChatRoomsController < ApplicationController

  layout false

  def index
    @allocation_tags_ids = (params[:allocation_tags_ids].class == String ? params[:allocation_tags_ids].split(",") : params[:allocation_tags_ids])
    authorize! :index, ChatRoom, on: @allocation_tags_ids
    @chat_rooms = ChatRoom.joins(academic_allocations: :allocation_tag).where(allocation_tags: {id: @allocation_tags_ids}).order("title").uniq
  end

  def new
    authorize! :create, ChatRoom, on: @allocation_tags_ids = params[:allocation_tags_ids]
    @chat_room = ChatRoom.new
    @chat_room.build_schedule(start_date: Date.current, end_date: Date.current)

    groups = Group.joins(:allocation_tag).where(allocation_tags: {id: [@allocation_tags_ids].flatten})
    @allocations = groups.map(&:students_participants).flatten.uniq
    @groups_codes = groups.map(&:code).uniq
  end

  def create
    authorize! :create, ChatRoom, on: @allocation_tags_ids = params[:allocation_tags_ids].split(" ")
    @chat_room = ChatRoom.new params[:chat_room]

    begin
      ChatRoom.transaction do
        @chat_room.save!
        @chat_room.academic_allocations.create @allocation_tags_ids.map {|at| {allocation_tag_id: at}}
      end
      render json: {success: true, notice: t(:created, scope: [:chat_rooms, :success])}
    rescue ActiveRecord::AssociationTypeMismatch
      render json: {success: false, alert: t(:not_associated)}, status: :unprocessable_entity
    rescue
      groups = Group.joins(:allocation_tag).where(allocation_tags: {id: [@allocation_tags_ids].flatten})
      @allocations = groups.map(&:students_participants).flatten.uniq
      @groups_codes = groups.map(&:code).uniq
      render :new
    end
  end

  def edit
    authorize! :update, ChatRoom, on: @allocation_tags_ids = params[:allocation_tags_ids].split(" ").flatten
    @chat_room = ChatRoom.find(params[:id])
    @schedule = @chat_room.schedule
    @allocations = Group.joins(:allocation_tag).where(allocation_tags: {id: @allocation_tags_ids}).map(&:students_participants).flatten.uniq
    @groups_codes = @chat_room.groups.map(&:code)
  end

  def update
    authorize! :update, ChatRoom, on: @allocation_tags_ids = params[:allocation_tags_ids].split(" ").flatten
    @chat_room = ChatRoom.find(params[:id])
    
    begin
      @chat_room.update_attributes!(params[:chat_room])

      render json: {success: true, notice: t(:updated, scope: [:chat_rooms, :success])}
    rescue ActiveRecord::AssociationTypeMismatch
      render json: {success: false, alert: t(:not_associated)}, status: :unprocessable_entity
    rescue
      @allocations = Group.joins(:allocation_tag).where(allocation_tags: {id: @allocation_tags_ids}).map(&:students_participants).flatten.uniq
      @groups_codes = @chat_room.groups.map(&:code)
      render :edit
    end

  end

  def destroy
    authorize! :destroy, ChatRoom, on: @allocation_tags_ids = params[:allocation_tags_ids]

    begin
      ChatRoom.transaction do
        @chat_rooms = ChatRoom.where(id: params[:id].split(","))
        raise "has_messages" if @chat_rooms.map(&:can_destroy?).include?(false)
        @chat_rooms.map(&:destroy)
      end
      render json: {success: true, notice: t(:deleted, scope: [:chat_rooms, :success])}
    rescue Exception => error
      alert_message = (error.message == "has_messages" ? t(:chat_has_messages, scope: [:chat_rooms, :error]) : t(:deleted, scope: [:chat_rooms, :error]))
      render json: {success: false, alert: alert_message}, status: :unprocessable_entity
    end

  end

end
